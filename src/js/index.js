const navBarBtn = document.querySelector('.navbar__button');
const navBarList = document.querySelector(".navbar__list");


navBarBtn.addEventListener('click', function (){
    if (navBarList.classList.contains('navbar__list-active')){
        navBarList.classList.remove('navbar__list-active')
    } else {
        navBarList.classList.add('navbar__list-active')
    }
})
